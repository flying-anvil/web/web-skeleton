<?php

declare(strict_types=1);

namespace FlyingAnvil\WebSkeleton;

class Index
{
    public static function greet(): void
    {
        echo sprintf(
            'Hello World from PHP %s.%s.%s',
            PHP_MAJOR_VERSION,
            PHP_MINOR_VERSION,
            PHP_RELEASE_VERSION,
        );

        echo '<br/>';

        echo sprintf(
            'Server time: %s',
            date(DATE_ATOM),
        );
    }
}
